#define _USE_MATH_DEFINES

#include "cercle.h"
#include <math.h>

void Cercle::SetCentre(Point centre) 
{
    this->centre = centre;
}

void Cercle::SetDiametre(int diametre) 
{
    this->diametre = diametre;
}

Point Cercle::GetCentre() 
{
    return this->centre;
}

int Cercle::GetDiametre() const 
{
    return this->diametre;
}

double Cercle::Perimetre() const 
{
    return (this->diametre * M_PI);
}

double Cercle::Surface() const 
{
    float rayon = this->diametre / 2;
    return ((rayon * rayon) * M_PI);
}

bool Cercle::DansLeCercle(Point p) const 
{
    bool dansLeCercle = false;
    float R = (diametre / 2);
    float dX = fabsf(p.x - this->centre.x);
    float dY = fabsf(p.y - this->centre.y);
    if ((dX > R) || (dY > R)) dansLeCercle = false;
    else dansLeCercle = true;
    return dansLeCercle;
};

bool Cercle::SurLeCercle(Point p) const 
{
    bool surLeCercle = false;
    float R = (diametre / 2);
    float dX = fabsf(p.x - this->centre.x);
    float dY = fabsf(p.y - this->centre.y);
    if ((dX == R) && (dY == R)) 
        surLeCercle = true;
    else 
        surLeCercle = false;
    return surLeCercle;
};

bool Cercle::PerimetrePlusGrand(Cercle c) const 
{
    if (this->Perimetre() > c.Perimetre()) 
        return true;
    else 
        return false;
}

bool Cercle::SurfacePlusGrande(Cercle c) const 
{
    if (this->Surface() > c.Surface()) 
        return true;
    else 
        return false;
}